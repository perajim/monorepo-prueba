package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sync"

	"github.com/gin-gonic/gin"
)

type apiResponse struct {
	IconUrl string `json:"icon_url"`
	Id string `json:"id"`
	Url string  `json:"url"`
	Value string  `json:"value"`
}

func main() {
	router := gin.Default()
	var wg sync.WaitGroup

	router.GET("/prueba", func(c *gin.Context){
		mapaResponse := make(map[string]apiResponse)
		for len(mapaResponse) < 25 {
			wg.Add(1)
			response, err := http.Get("https://api.chucknorris.io/jokes/random")
			if err != nil {
				fmt.Println("Hubo un error")
			}

			var data apiResponse
			err = json.NewDecoder(response.Body).Decode(&data)
			if err != nil {
				fmt.Println("Error en decode")
			}
			mapaResponse[data.Id] = data
			wg.Done()
		}
		wg.Wait()
		c.JSON(http.StatusOK, mapaResponse)
	})
	router.Run(":3001")
}