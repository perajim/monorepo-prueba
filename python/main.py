from http.server import BaseHTTPRequestHandler, HTTPServer
import json
import requests
class DataResponse:
    def __init__(self, iconUrl, id, url, value):
        self.IconUrl = iconUrl
        self.Id = id
        self.Url = url
        self.Value = value
        
class JSONHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == '/prueba':
            api_url = 'https://api.chucknorris.io/jokes/random'

            json_data = None
            mapResponse = dict()

            while len(mapResponse) < 25:
                try:
                    response = requests.get(api_url)
                    if response.status_code == 200:
                        json_data = response.json()
                        data_object = DataResponse(json_data['icon_url'], json_data['id'], json_data['url'], json_data['value'])
                        mapResponse[data_object.Id] = data_object
                        print(data_object.IconUrl)
                        print(data_object.Id)
                        print(data_object.Url)
                        print(data_object.Value)
                        #return json_data
                    else:
                        print('Error en la solicitud a la API:', response.status_code)
                        return None

                except requests.exceptions.RequestException as e:
                    print('Error en la solicitud a la API:', str(e))
                    return None
            
            self.send_response(200)
            self.send_header('Content-type', 'application/json')
            self.end_headers()
            self.wfile.write(json.dumps(mapResponse, default=lambda o: o.__dict__).encode())


        else:
            self.send_response(404)
            self.send_header('Content-type', 'application/json')
            self.end_headers()
            self.wfile.write(json.dumps({'error': 'Endpoint no encontrado'}).encode())

def run_server():
    host = 'localhost'
    port = 8000

    server = HTTPServer((host, port), JSONHandler)
    print(f'Server running on {host}:{port}')

    server.serve_forever()

if __name__ == '__main__':
    run_server()
